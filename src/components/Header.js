import classes from './Header.module.css';
import { useSelector } from 'react-redux';
import { authActions } from '../store/auth';
import { useDispatch } from 'react-redux';


const Header = () => {
  const isRegistered = useSelector(state=>state.auth.isRegistered);
  const dispatch = useDispatch();
  const logoutHandler = ()=>{
    dispatch(authActions.logout());
  };
  
  return (
    <header className={classes.header}>
      <h1>Redux Auth</h1>
      { isRegistered && <nav>
         <ul>
          <li>
            <a href='/'>My Products</a>
          </li>
          <li>
            <a href='/'>My Sales</a>
          </li>
          <li>
            <button onClick={logoutHandler}>Logout</button>
          </li>
        </ul>
      </nav>}
    </header>
  );
};

export default Header;
