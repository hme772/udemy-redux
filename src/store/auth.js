import {createSlice} from '@reduxjs/toolkit';

const initialAuthState = {isRegistered:false,}
const authSlice = createSlice({
  name:'auth',
  initialState:initialAuthState,
  reducers:
  {
    login(state){
      state.isRegistered=true;
    },
    logout(state){
      state.isRegistered=false;
    },
  }
});
export const authActions = authSlice.actions;

export default authSlice.reducer;